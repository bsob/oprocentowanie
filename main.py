from datetime import datetime
from calendar import month_abbr

miesiace = ['Styczen', 'Luty', 'Marzec', 'Kwiecien', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpien', 'Wrzesien', 'Pazdziernik', 'Listopad', 'Grudzien']
aktualny_miesiac = datetime.now().month
lista_miesiecy=[miesiace[(aktualny_miesiac + i) % 12] for i in range(24)]


inflacja=[1.592824484,-0.453509101,2.324671717,1.261254407,1.782526286,2.329384541,1.502229842,1.782526286,2.328848994,0.616921348,2.352295886,0.337779545,1.577035247,-0.292781443,2.48619659,0.267110318,1.417952672,1.054243267,1.480520104,1.577035247,-0.07742069,1.165733399,-0.404186718,1.499708521]


kwota_kredytu=int(input("Prosze podaj kwote kredytu?: \n"))
oprocontowanie=float(input("Prosze podaj oprecontowanie kredytu?: \n"))
rata=float(input("Prosze podaj kwote raty kredytu?: \n"))


i=0
pozyczka=kwota_kredytu
for miesiac in lista_miesiecy:
    pozyczka_poprzedni_miesiac=pozyczka
    pozyczka=(1+((inflacja[i]+oprocontowanie)/(kwota_kredytu/10)))*pozyczka-rata
    mniej=pozyczka_poprzedni_miesiac-pozyczka
    print(f"{miesiac}:\n" + f"Twoja pozostała kwota kredytu to {pozyczka}, to {mniej} mniej niż w poprzednim miesiącu.")
    i=i+1